<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div>Key:</div>
<div class="keyPhrase">${linkData.originalKey}</div>
<c:if test="${!empty linkData.links}">
    <div>Links:</div>
    <c:forEach var="link" items="${linkData.links}">
        <div><a href="${link}">${link}</a></div>
    </c:forEach>
</c:if>
<c:if test="${empty linkData.links}">
    <div>Key phrase is reserved, but there are no links attached to it yet. Please wait, or ping the link author.</div>
    <div class="hint">Hint: bookmark this page so you'll not miss this link later.</div>
</c:if>
<br/>
<br/>
<form action="editLink.jsp">
    <input type="hidden" name="id" value="${linkData.ID}">
    <c:if test="${!empty linkData.passwordHash}">
        <label for="password">Password:</label><br/>
        <input id="password" type="password" name="password"/>
    </c:if>
    <input type="submit" name="edit" value="Edit Link"/>
</form>
