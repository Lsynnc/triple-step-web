package com.triplestep.service;

import com.triplestep.data.LinkData;

import java.io.IOException;
import java.util.Collection;

/**
 * @author Valentyn Markovych
 *         Created on 27.03.2016
 */
public interface PersistenceService extends Service {

    Collection<LinkData> loadAllLinks();

    void saveLink(LinkData linkData) throws IOException;
}
