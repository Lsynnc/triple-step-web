package com.triplestep.config.impl;

import com.triplestep.config.ConfigService;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author Valentyn Markovych
 *         Created on 27.03.2016
 */
public class DefaultConfigService extends Properties implements ConfigService {

    protected final String DEFAULT_APPLICATION_DIR = "triple-step.data";
    protected final String DEFAULT_SETTINGS_DIR = "settings";
    protected final String DEFAULT_DATA_DIR = "data";

    protected final String DEFAULT_SETTINGS_FILE = "settings.properties";

    protected String settingsDir = DEFAULT_SETTINGS_DIR + '/';
    protected String dataDir = DEFAULT_DATA_DIR + '/';
    protected String settingsFile = DEFAULT_SETTINGS_FILE;

    protected Map<String, Object> objectProperties;


    public DefaultConfigService() {

        try {
            objectProperties = new HashMap<>();
            init();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }


    private void init() throws IOException {

        setUserSettingsDir();
        setDataDir();
        ensureFileExists();
        readProperties();
    }


    protected void setDataDir() {

        File dataDirFile = new File(dataDir);
        final boolean isExist = dataDirFile.exists();
        if (!isExist) {
            dataDirFile.mkdirs();
        }
        setObject(ConfigService.SHORTEST_KEY_LENGTH, dataDirFile);
    }

    @Override
    public void setObject(String key, Object value) {

        objectProperties.put(key, value);
        // TODO check if we should save properties immediately
    }


    @Override
    public Object getObject(String key) {

        return objectProperties.get(key);
    }


    private void setUserSettingsDir() {

        final String homeDir = System.getProperty("user.home");
        if (homeDir != null && !homeDir.isEmpty()) {
            settingsDir = homeDir + '/' + DEFAULT_APPLICATION_DIR + '/' + DEFAULT_SETTINGS_DIR + '/';
            dataDir = homeDir + '/' + DEFAULT_APPLICATION_DIR + '/' + DEFAULT_DATA_DIR + '/';
        }
    }


    private void readProperties() throws IOException {

        FileReader fileReader = new FileReader(settingsDir + settingsFile);
        load(fileReader);
    }


    public void saveProperties() throws IOException {

        FileWriter fileWriter = new FileWriter(settingsDir + settingsFile);
        super.store(fileWriter, "StoredProperties");
    }


    private void ensureFileExists() throws IOException {

        File propertiesFile = new File(settingsDir + settingsFile);
        final boolean isExist = propertiesFile.exists();
        if (!isExist) {
            propertiesFile.getParentFile().mkdirs();
            propertiesFile.createNewFile();
        }
    }
}
