package com.triplestep.data;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author Valentyn Markovych
 *         Created on 26.03.2016
 */
public class LinkData implements Serializable {

    private static final long serialVersionUID = -2422083860265001222L;

    protected final UUID ID;

    // WARNING! Do NOT change fields type, as old serializable objects can NOT be loaded after this.
    protected Date createDate;
    protected Date editDate;

    protected String originalKey;
    protected String key;
    protected byte[] passwordHash;

    protected List<String> links;


    public LinkData() {

        ID = UUID.randomUUID();
    }


    @Override
    public String toString() {

        return "LinkData{" +
                "ID=" + ID +
                ", createDate=" + createDate +
                ", editDate=" + editDate +
                ", originalKey='" + originalKey + '\'' +
                ", key='" + key + '\'' +
                ", passwordHash=" + (passwordHash == null ? null : "******") +
                ", links=" + links +
                '}';
    }


    public UUID getID() {

        return ID;
    }


    public byte[] getPasswordHash() {

        return passwordHash;
    }


    public void setPasswordHash(byte[] passwordHash) {

        this.passwordHash = passwordHash;
    }


    public Date getCreateDate() {

        return createDate;
    }


    public void setCreateDate(Date createDate) {

        this.createDate = createDate;
    }


    public Date getEditDate() {

        return editDate;
    }


    public void setEditDate(Date editDate) {

        this.editDate = editDate;
    }


    public String getOriginalKey() {

        return originalKey;
    }


    public void setOriginalKey(String originalKey) {

        this.originalKey = originalKey;
    }


    public String getKey() {

        return key;
    }


    public void setKey(String key) {

        this.key = key;
    }


    public List<String> getLinks() {

        return links;
    }


    public void setLinks(List<String> links) {

        this.links = links;
    }
}
