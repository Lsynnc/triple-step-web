package com.triplestep.error.impl;

import com.triplestep.error.ErrorCode;
import com.triplestep.error.ErrorData;
import com.triplestep.error.ErrorService;

/**
 * @author Valentyn Markovych
 *         Created on 13.04.2016
 */
public class DefaultErrorService implements ErrorService {

    @Override
    public ErrorData getLocalizedError(String errorCodeString) {

        ErrorCode errorCode = null;
        try {
            int errorCodeNumber = Integer.parseInt(errorCodeString);
            errorCode = ErrorCode.getByCode(errorCodeNumber);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (errorCode == null) {
            errorCode = ErrorCode.SERVER_ERROR;
        }

        ErrorData errorData = new ErrorData();
        errorData.setErrorCode(errorCode);
        errorData.setMessage(errorCode.getDescription()); // TODO replace this to localized string message
        return errorData;
    }
}
