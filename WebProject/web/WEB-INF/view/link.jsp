<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" href="/static/css/main.css"/>
    <title>${title}</title>
</head>
<body>
<a href="/" class="menu">Home</a><a href="instruction.jsp" class="menu">How it works</a>
<h1>${title}</h1>
<c:if test="${!empty linkData}">
    <%@ include file="link/linkData.jsp" %>
</c:if>
<c:if test="${empty linkData}">
    <form action="link.jsp" method="get">
        <div>No links found for key '${originalKey}'. Try another one, or reserve this phrase for future use:</div>
        <label for="getLinkKey">Key phrase:</label><br/>
        <input id="getLinkKey" type="text" name="key" value="${originalKey}"/>
        <input type="submit" value="Get Link"/>
    </form>
    <form action="reserveLink.jsp" method="post">
        <input type="hidden" name="originalKey" value="${originalKey}"/>
        <input type="submit" value="Reserve phrase"/>
    </form>
</c:if>
<hr/>
<%@ include file="global/copyright.jsp" %>
</body>
</html>
