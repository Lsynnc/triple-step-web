package com.triplestep.data.impl;

import com.triplestep.data.ConversionService;
import com.triplestep.data.Converter;
import com.triplestep.data.LinkData;
import com.triplestep.service.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Valentyn Markovych
 *         Created on 31.03.2016
 */
public class DefaultConversionService implements ConversionService {

    protected Map<ClassSet, Converter> converters;


    public DefaultConversionService() {

        converters = new HashMap<>();
        setComverters();
    }


    protected void setComverters() {

        addConverter(Map.class, LinkData.class, new DefaultLinkConverter());
    }


    protected void addConverter(Class<?> sourceClass, Class<?> targetClass, Converter converter) {

        ClassSet classSet = new ClassSet(sourceClass, targetClass);
        converters.put(classSet, converter);
    }


    @Override
    public <FROM, TO> Converter getConverter(Class<FROM> SourceClass, Class<TO> TargetClass) {

        ClassSet classSet = new ClassSet(SourceClass, TargetClass);
        return converters.get(classSet);
    }


    protected class ClassSet {

        protected Class from;
        protected Class to;


        public ClassSet(Class<?> from, Class<?> to) {

            this.from = from;
            this.to = to;
        }


        @Override
        public boolean equals(Object o) {

            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ClassSet classSet = (ClassSet) o;

            if (!from.equals(classSet.from)) return false;
            return to.equals(classSet.to);

        }


        @Override
        public int hashCode() {

            int result = from.hashCode();
            result = 31 * result + to.hashCode();
            return result;
        }
    }
}
