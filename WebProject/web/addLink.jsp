<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" href="/static/css/main.css"/>
    <title>${title}</title>
</head>
<body>
<a href="/" class="menu">Home</a>
<h1>Add New Link</h1>
<form action="link.do" method="post">
    <label for="key"><strong>Key phrase:*</strong></label><br/>
    <input id="key" type="text" name="key" value="${param.originalKey}"/><br/>
    <label for="link">Link:</label><br/>
    <input id="link" type="text" name="link"/><br/>
    <%--<label for="pass">Password (lock link):</label><br/>--%>
    <%--<input id="pass" type="password" name="pass"/><br/>--%>
    <input type="submit" value="Add Link">
</form>
<hr/>
<%@ include file="WEB-INF/view/global/copyright.jsp" %>
</body>
</html>
