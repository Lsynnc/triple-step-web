package com.triplestep.data.impl;

import com.triplestep.config.WebConstants;
import com.triplestep.data.Converter;
import com.triplestep.data.LinkData;
import com.triplestep.util.WebUtil;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * @author Valentyn Markovych
 *         Created on 26.03.2016
 */
public class DefaultLinkConverter implements Converter<Map<String, String[]>, LinkData> {


    @Override
    public LinkData convert(Map<String, String[]> parameters) {

        LinkData linkData = new LinkData();

        final ArrayList<String> links = new ArrayList<>();
        final String[] linkStrings = parameters.get(WebConstants.PARAM_LINK);
        if (linkStrings != null) {
            for (String linkString : linkStrings) {
                links.add(linkString);
            }
        }
        linkData.setLinks(links);
        linkData.setCreateDate(new Date());
        linkData.setOriginalKey(WebUtil.getFirst(parameters, WebConstants.PARAM_KEY));
        String password = WebUtil.getFirst(parameters, WebConstants.PARAM_PASS);
        if (password != null && !password.isEmpty()) {
            MessageDigest messageDigest = null;
            try {
                messageDigest = MessageDigest.getInstance("SHA-256"); // TODO Move to settings
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            linkData.setPasswordHash(messageDigest.digest());
        }
        System.out.println(linkData);
        return linkData;
    }
}
