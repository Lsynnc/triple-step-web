<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" href="/static/css/main.css"/>
    <title>${title}</title>
</head>
<body>
<a href="/" class="not-active menu">Home</a><a href="addLink.jsp" class="menu">Add new link</a><a
        href="instruction.jsp" class="menu">How it works</a>
<h1>${title}</h1>
<div>Type in a key phrase below and press Get Link button</div>
<form action="link.jsp" method="get">
    <div>
        <label for="getLinkKey">Key phrase:</label><br/>
        <input id="getLinkKey" type="text" name="key"/><input type="submit" value="Get Link">
    </div>
</form>
<hr/>
<%@ include file="global/copyright.jsp" %>
</body>
</html>
