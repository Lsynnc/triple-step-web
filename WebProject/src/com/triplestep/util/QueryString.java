package com.triplestep.util;

/**
 * @author Valentyn Markovych
 * Created on 01.04.2016
 * Source: http://stackoverflow.com/questions/1861620/is-there-a-java-package-to-handle-building-urls
 */

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

public class QueryString {

    private String query = "";


    public QueryString(Map<String, String> map) {

        final Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pairs = it.next();
            try {
                query += URLEncoder.encode(pairs.getKey(), "utf-8") + "=" + URLEncoder.encode(pairs.getValue(), "utf-8");
                if (it.hasNext()) {
                    query += "&";
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }


    public QueryString(Object name, Object value) {

        try {
            query = URLEncoder.encode(name.toString(), "utf-8") + "=" + URLEncoder.encode(value.toString(), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


    public QueryString() {

        query = "";
    }


    public synchronized void add(Object name, Object value) {

        if (!query.trim().equals("")) query += "&";
        try {
            query += URLEncoder.encode(name.toString(), "utf-8") + "=" + URLEncoder.encode(value.toString(), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


    public String toString() {

        return query;
    }
}