package com.triplestep.servlet;

import com.triplestep.config.WebConstants;
import com.triplestep.data.LinkData;
import com.triplestep.service.LinkService;
import com.triplestep.service.ServiceManager;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

/**
 * @author Valentyn Markovych
 *         Created on 23.03.2016
 */
@WebServlet(urlPatterns = WebConstants.LINKS_PAGE)
public class LinksPageServlet extends HttpServlet {


    protected ServiceManager serviceManager;
    protected LinkService linkService;


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }


    /**
     * Temporary method for development reasons. Displays all current links.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // FIXME this method should be secured or removed

        request.setAttribute("title", "Links");

        final Collection<LinkData> linkDatas = linkService.getAll();
        request.setAttribute("linkDatas", linkDatas);

        request.getRequestDispatcher(WebConstants.LINKS_VIEW).forward(request, response);
    }


    @Override
    public void init() {

        serviceManager = ServiceManager.getInstance();
        linkService = serviceManager.getService(LinkService.class);
    }
}
