package com.triplestep.error;


import com.triplestep.service.Service;

/**
 * @author Valentyn Markovych
 *         Created on 11.04.2016
 */
public interface ErrorService extends Service {

    ErrorData getLocalizedError(String errorCode);
}
