package com.triplestep.error;

/**
 * @author Valentyn Markovych
 *         Created on 11.04.2016
 */
public class ErrorData {

    protected ErrorCode errorCode;
    protected String message;

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
