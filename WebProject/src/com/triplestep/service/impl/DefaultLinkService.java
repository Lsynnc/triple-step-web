package com.triplestep.service.impl;

import com.triplestep.data.LinkData;
import com.triplestep.service.*;

import java.io.IOException;
import java.util.Collection;

/**
 * @author Valentyn Markovych
 *         Created on 01.04.2016
 */
public class DefaultLinkService implements LinkService {

    protected ServiceManager serviceManager;
    protected PersistenceService persistenceService;
    protected SearchService searchService;
    protected LinkDataProcessorService linkDataProcessorService;


    public DefaultLinkService() {

        serviceManager = ServiceManager.getInstance();
        persistenceService = serviceManager.getService(PersistenceService.class);
        searchService = serviceManager.getService(SearchService.class);
        linkDataProcessorService = serviceManager.getService(LinkDataProcessorService.class);
    }


    @Override
    public void addLink(LinkData linkData) {

        linkDataProcessorService.preprocessNewLink(linkData);
        try {
            persistenceService.saveLink(linkData);
        } catch (IOException e) {
            e.printStackTrace();
        }
        searchService.addLink(linkData);
    }


    @Override
    public LinkData getLinkByKey(String keyPhrase) {

        keyPhrase = linkDataProcessorService.cleanKey(keyPhrase);
        return searchService.findSimilarLinks(keyPhrase);
    }


    @Override
    public LinkData getLinkById(String linkId) {

        if (linkId == null || linkId.isEmpty()) {
            return null;
        }

        try {
            final LinkData link = searchService.getLink(linkId);
            return link;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    public Collection<LinkData> getAll() {

        return searchService.getAll();
    }


    @Override
    public void updateLink(LinkData linkData, LinkData newlinkData) {

        linkDataProcessorService.cleanLinks(newlinkData);
        linkData.setLinks(newlinkData.getLinks());
        try {
            persistenceService.saveLink(linkData);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
