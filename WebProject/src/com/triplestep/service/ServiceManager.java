package com.triplestep.service;

import com.triplestep.config.ConfigService;
import com.triplestep.config.impl.DefaultConfigService;
import com.triplestep.data.impl.DefaultConversionService;
import com.triplestep.error.impl.DefaultErrorService;
import com.triplestep.service.impl.DefaultLinkDataProcessorService;
import com.triplestep.service.impl.DefaultLinkService;
import com.triplestep.service.impl.LevenshteinSearchService;
import com.triplestep.service.impl.SerializablePersistenceService;

import java.util.*;

/**
 * @author Valentyn Markovych
 *         Created on 28.03.2016
 */
public class ServiceManager {

    private static ServiceManager instance = new ServiceManager();

    protected Map<Class<? extends Service>, Service> loadedServices;
    protected Set<Class<? extends Service>> knownServices;
    protected ConfigService configService;


    private ServiceManager() {

        loadedServices = new HashMap<>();
        knownServices = new HashSet<>();
        initConfigService();
        setKnownServices();
    }


    private void initConfigService() {

        knownServices.add(DefaultConfigService.class);
        configService = getService(ConfigService.class);
    }


    private void setKnownServices() {

        // TODO Classes can be moved to config
        knownServices.add(DefaultConversionService.class);
        knownServices.add(DefaultLinkService.class);
        knownServices.add(SerializablePersistenceService.class);
        knownServices.add(LevenshteinSearchService.class);
        knownServices.add(DefaultLinkDataProcessorService.class);
        knownServices.add(DefaultErrorService.class);
    }


    public static ServiceManager getInstance() {

        return instance;
    }


    public <T> T getService(Class<? extends Service> serviceClass) {

        Service service = loadedServices.get(serviceClass);
        if (service == null) {
            service = loadAndGet(serviceClass);
        }
        return (T) service;
    }


    private Service loadAndGet(Class<? extends Service> serviceClass) {

        final Iterator<Class<? extends Service>> knownServiceIterator = knownServices.iterator();
        while (knownServiceIterator.hasNext()) {
            final Class<? extends Service> knownServiceClass = knownServiceIterator.next();
            if (serviceClass.isAssignableFrom(knownServiceClass)) {
                try {
                    final Service service = knownServiceClass.newInstance();
                    loadedServices.put(serviceClass, service);
                    return service;
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
