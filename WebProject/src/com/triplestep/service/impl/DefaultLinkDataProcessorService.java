package com.triplestep.service.impl;

import com.triplestep.data.LinkData;
import com.triplestep.service.LinkDataProcessorService;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * @author Valentyn Markovych
 *         Created on 02.04.2016
 */
public class DefaultLinkDataProcessorService implements LinkDataProcessorService {

    @Override
    public void preprocessNewLink(LinkData linkData) {

        cleanOriginalKey(linkData);
        setKey(linkData);
        cleanLinks(linkData);
    }

    @Override
    public void cleanLinks(LinkData linkData) {

        final List<String> links = linkData.getLinks();
        if (links != null && !links.isEmpty()) {
            final ListIterator<String> linksIterator = links.listIterator();
            while (linksIterator.hasNext()) {
                String link = cleanLink(linksIterator.next());
                if (link != null) {
                    linksIterator.set(link);
                } else {
                    linksIterator.remove();
                }
            }
        }
    }


    private String cleanLink(String link) {

        // TODO XSS, ...?

        if (link == null) {
            return null;
        }

        final String cleanLink = link.trim();
        if (!cleanLink.isEmpty()) {
            return cleanLink;
        } else {
            return null;
        }
    }


    private void setKey(LinkData linkData) {

        final String key = cleanKey(linkData.getOriginalKey());
        linkData.setKey(key);
    }

    public String cleanKey(String keyPhrase) {

        keyPhrase = keyPhrase.replaceAll("[^\\p{L}0-9_]", ""); // Remove anything non-significant
        keyPhrase = keyPhrase.toLowerCase();
        return keyPhrase;
    }


    private void cleanOriginalKey(LinkData linkData) {

        String newOriginalKey = linkData.getOriginalKey().trim();
        newOriginalKey = newOriginalKey.replaceAll("\\s{2,}", " "); // Collapse groups of spaces to one
        newOriginalKey = newOriginalKey.replaceAll("(.)\\1+", "$1$1"); // Collapse groups of characters to two

        linkData.setOriginalKey(newOriginalKey);
    }
}
