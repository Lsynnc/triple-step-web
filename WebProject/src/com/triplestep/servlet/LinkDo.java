package com.triplestep.servlet;

import com.triplestep.config.WebConstants;
import com.triplestep.data.ConversionService;
import com.triplestep.data.Converter;
import com.triplestep.data.LinkData;
import com.triplestep.service.LinkService;
import com.triplestep.service.ServiceManager;
import com.triplestep.util.QueryString;
import com.triplestep.util.WebUtil;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Valentyn Markovych
 *         Created on 26.03.2016
 */
@WebServlet(urlPatterns = {"/link.do"})
public class LinkDo extends HttpServlet {

    protected ServiceManager serviceManager;
    protected ConversionService conversionService;
    protected LinkService linkService;
    protected Converter<Map, LinkData> mapToLinkConverter;


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        final Map<String, String[]> parameterMap = request.getParameterMap();
        final LinkData linkData = mapToLinkConverter.convert(parameterMap);
        linkService.addLink(linkData);

        Map<String, String> redirectParametersMap = new HashMap<>();
        redirectParametersMap.put(WebConstants.PARAM_KEY, linkData.getKey());

        QueryString queryString = new QueryString(redirectParametersMap);
        response.sendRedirect(WebConstants.LINK_PAGE + '?' + queryString);
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }


    @Override
    public void init() {

        serviceManager = ServiceManager.getInstance();
        conversionService = serviceManager.getService(ConversionService.class);
        linkService = serviceManager.getService(LinkService.class);
        mapToLinkConverter = conversionService.getConverter(Map.class, LinkData.class);
    }
}
