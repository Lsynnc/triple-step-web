package com.triplestep.servlet;

import com.triplestep.config.WebConstants;
import com.triplestep.data.ConversionService;
import com.triplestep.data.Converter;
import com.triplestep.data.LinkData;
import com.triplestep.error.ErrorCode;
import com.triplestep.error.ErrorData;
import com.triplestep.error.ErrorService;
import com.triplestep.service.LinkService;
import com.triplestep.service.ServiceManager;
import com.triplestep.util.QueryString;
import com.triplestep.util.WebUtil;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Valentyn Markovych
 *         Created on 23.03.2016
 */
@WebServlet(urlPatterns = WebConstants.EDIT_LINK_PAGE)
public class LinkEditPageServlet extends HttpServlet {


    protected ServiceManager serviceManager;
    protected LinkService linkService;
    protected ErrorService errorService;
    protected Converter<Map, LinkData> mapToLinkConverter;
    protected ConversionService conversionService;


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        // Get link
        String linkId = request.getParameter(WebConstants.PARAM_ID);
        final LinkData linkData = linkService.getLinkById(linkId);
        if (linkData == null) {
            QueryString queryString = WebUtil.createErrorsList(ErrorCode.SERVER_ERROR);
            if (linkId != null) {
                queryString.add(WebConstants.PARAM_ID, linkId);
            }
            response.sendRedirect(WebConstants.EDIT_LINK_PAGE + '?' + queryString);
        }

        // TODO Check security

        final Map<String, String[]> parameterMap = request.getParameterMap();
        Map<String, String> redirectParametersMap = new HashMap<>();

        if (parameterMap.get(WebConstants.PARAM_BACK) != null) {
            // Return without update
            redirectParametersMap.put(WebConstants.PARAM_KEY, linkData.getKey());
            QueryString queryString = new QueryString(redirectParametersMap);
            response.sendRedirect(WebConstants.LINK_PAGE + '?' + queryString);
            return;
        }

        // Update link
        final LinkData newlinkData = mapToLinkConverter.convert(parameterMap);
        linkService.updateLink(linkData, newlinkData);

        redirectParametersMap.put(WebConstants.PARAM_ID, linkData.getID().toString());
        QueryString queryString = new QueryString(redirectParametersMap);
        response.sendRedirect(WebConstants.EDIT_LINK_PAGE + '?' + queryString);
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("title", "Edit Link");

        if (hasErrors(request)) {
            request.setAttribute("errors", createErrorsList(request.getParameterValues(WebConstants.PARAM_ERROR)));
            request.getRequestDispatcher(WebConstants.EDIT_LINK_VIEW).forward(request, response);
            return;
        }

        String linkId = WebUtil.sanitizeInput(request.getParameter(WebConstants.PARAM_ID));

        if (linkId != null && !linkId.isEmpty()) {
            final LinkData linkData = linkService.getLinkById(linkId);
            if (linkData != null) {
                request.setAttribute("linkData", linkData);
                request.getRequestDispatcher(WebConstants.EDIT_LINK_VIEW).forward(request, response);
                return;
            }
        }

        QueryString queryString = WebUtil.createErrorsList(ErrorCode.SERVER_ERROR);
        response.sendRedirect(WebConstants.EDIT_LINK_PAGE + '?' + queryString);
    }

    private List<ErrorData> createErrorsList(String[] errorCodes) {

        List<ErrorData> errorDatas = new ArrayList<>();

        for (String errorCode : errorCodes) {
            ErrorData errorData = errorService.getLocalizedError(errorCode);
            errorDatas.add(errorData);
        }
        return errorDatas;
    }


    private boolean hasErrors(HttpServletRequest request) {

        final String[] parameterValues = request.getParameterValues(WebConstants.PARAM_ERROR);
        if (parameterValues != null && parameterValues.length > 0) {
            return true;
        }

        return false;
    }


    @Override
    public void init() {

        serviceManager = ServiceManager.getInstance();
        linkService = serviceManager.getService(LinkService.class);
        errorService = serviceManager.getService(ErrorService.class);
        conversionService = serviceManager.getService(ConversionService.class);
        mapToLinkConverter = conversionService.getConverter(Map.class, LinkData.class);
    }
}
