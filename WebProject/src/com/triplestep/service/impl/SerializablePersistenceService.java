package com.triplestep.service.impl;

import com.triplestep.config.ConfigService;
import com.triplestep.data.LinkData;
import com.triplestep.service.PersistenceService;
import com.triplestep.service.ServiceManager;

import java.io.*;
import java.util.Collection;
import java.util.HashSet;

/**
 * @author Valentyn Markovych
 *         Created on 27.03.2016
 */
public class SerializablePersistenceService implements PersistenceService {

    protected ConfigService configService;
    protected File dataDir;


    public SerializablePersistenceService() {

        configService = ServiceManager.getInstance().getService(ConfigService.class);
        dataDir = (File) configService.getObject(ConfigService.SHORTEST_KEY_LENGTH);
    }


    @Override
    public Collection<LinkData> loadAllLinks() {

        File[] files = dataDir.listFiles();
        Collection<LinkData> loadedLinks = new HashSet<>();
        loadLinkData(files, loadedLinks);
        return loadedLinks;
    }


    @Override
    public void saveLink(LinkData linkData) throws IOException {

        FileOutputStream fileOutputStream = new FileOutputStream(getFileName(linkData));
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(linkData);
        objectOutputStream.close();
        fileOutputStream.close();
    }


    private String getFileName(LinkData linkData) {

        return dataDir.getPath() + '/' + linkData.getID();
    }


    public static void loadLinkData(File[] files, Collection<LinkData> loadedLinks) {

        for (File file : files) {
            if (file.isDirectory()) {
                System.out.println("Directory: " + file.getName());
                loadLinkData(file.listFiles(), loadedLinks);
            } else {
                System.out.println("File: " + file.getName());
                final LinkData linkData = loadLink(file);
                if (linkData != null) {
                    loadedLinks.add(linkData);
                }
            }
        }
    }


    private static LinkData loadLink(File file) {

        LinkData link = null;
        try (FileInputStream f_in = new FileInputStream(file)) {
            ObjectInputStream obj_in = new ObjectInputStream(f_in);
            link = (LinkData) obj_in.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return link;
    }
}
