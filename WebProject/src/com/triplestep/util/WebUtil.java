package com.triplestep.util;

import com.triplestep.config.WebConstants;
import com.triplestep.error.ErrorCode;

import java.util.Map;

/**
 * @author Valentyn Markovych
 *         Created on 26.03.2016
 */
public class WebUtil {

    public static String getFirst(Map<String, String[]> parameters, String key, String defaultValue) {

        String[] val = parameters.get(key);
        if (val != null && val.length > 0) {
            return val[0];
        } else {
            return defaultValue;
        }
    }


    public static String getFirst(Map<String, String[]> parameters, String key) {

        return getFirst(parameters, key, null);
    }


    public static String sanitizeInput(String textField) {

        if (textField == null) {
            return null;
        }

        // TODO Implement method
        textField = textField.replaceAll("\\s\1+", " ").trim();

        return textField;
    }


    public static QueryString createErrorsList(ErrorCode... errorCodes) {

        QueryString queryString = new QueryString();
        for (ErrorCode errorCode : errorCodes) {
            queryString.add(WebConstants.PARAM_ERROR, errorCode.getCode());
        }
        return queryString;
    }
}
