package com.triplestep.service;

import com.triplestep.data.LinkData;

/**
 * @author Valentyn Markovych
 *         Created on 02.04.2016
 */
public interface LinkDataProcessorService extends Service {

    void preprocessNewLink(LinkData linkData);

    void cleanLinks(LinkData linkData);

    String cleanKey(String keyPhrase);
}
