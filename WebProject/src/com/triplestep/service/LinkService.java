package com.triplestep.service;

import com.triplestep.data.LinkData;

import java.util.Collection;

/**
 * @author Valentyn Markovych
 *         Created on 01.04.2016
 */
public interface LinkService extends Service {

    void addLink(LinkData linkData);


    LinkData getLinkByKey(String keyPhrase);


    LinkData getLinkById(String linkId);


    Collection<LinkData> getAll();


    void updateLink(LinkData linkData, LinkData newlinkData);
}
