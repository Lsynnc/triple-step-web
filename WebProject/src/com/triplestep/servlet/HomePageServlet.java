package com.triplestep.servlet;

import com.triplestep.config.WebConstants;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Valentyn Markovych
 *         Created on 23.03.2016
 */
@WebServlet(urlPatterns = WebConstants.HOME_PAGE)
public class HomePageServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("title", "Get Link");

        request.getRequestDispatcher(WebConstants.HOME_VIEW).forward(request, response);
    }
}
