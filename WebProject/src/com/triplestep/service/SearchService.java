package com.triplestep.service;

import com.triplestep.data.LinkData;

import java.util.Collection;

/**
 * @author Valentyn Markovych
 *         Created on 01.04.2016
 */
public interface SearchService extends Service {

    void addLink(LinkData linkData);


    LinkData findSimilarLinks(String keyPhrase);


    Collection<LinkData> getAll();


    LinkData getLink(String linkId);
}
