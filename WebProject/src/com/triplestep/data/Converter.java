package com.triplestep.data;

/**
 * @author Valentyn Markovych
 *         Created on 31.03.2016
 */
public interface Converter<FROM, TO> {


    TO convert(FROM source);
}
