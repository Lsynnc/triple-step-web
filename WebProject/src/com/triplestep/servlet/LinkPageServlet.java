package com.triplestep.servlet;

import com.triplestep.config.WebConstants;
import com.triplestep.data.LinkData;
import com.triplestep.service.LinkService;
import com.triplestep.service.ServiceManager;
import com.triplestep.util.WebUtil;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Valentyn Markovych
 *         Created on 23.03.2016
 */
@WebServlet(urlPatterns = WebConstants.LINK_PAGE)
public class LinkPageServlet extends HttpServlet {


    protected ServiceManager serviceManager;
    protected LinkService linkService;


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("title", "Link Info");

        String keyPhrase = request.getParameter(WebConstants.PARAM_KEY);
        keyPhrase = WebUtil.sanitizeInput(keyPhrase);

        if (keyPhrase != null && !keyPhrase.isEmpty()) {
            final LinkData linkData = linkService.getLinkByKey(keyPhrase);
            if (linkData != null) {
                request.setAttribute("linkData", linkData);
            } else {
                request.setAttribute("originalKey", keyPhrase);
            }
            request.getRequestDispatcher(WebConstants.LINK_VIEW).forward(request, response);
        } else {
            response.sendRedirect(WebConstants.HOME_PAGE);
        }

    }


    @Override
    public void init() {

        serviceManager = ServiceManager.getInstance();
        linkService = serviceManager.getService(LinkService.class);
    }
}
