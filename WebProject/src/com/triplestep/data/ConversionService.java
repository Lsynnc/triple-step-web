package com.triplestep.data;

import com.triplestep.service.Service;

/**
 * @author Valentyn Markovych
 *         Created on 31.03.2016
 */
public interface ConversionService extends Service {

    <FROM, TO>Converter getConverter(Class<FROM> SourceClass, Class<TO> TargetClass);
}
