<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%
    request.setCharacterEncoding("UTF-8");
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" href="/static/css/main.css"/>
    <title>${title}</title>
</head>
<body>
<a href="/" class="menu">Home</a><a href="instruction.jsp" class="menu">How it works</a>
<hr/>
<h1>Link reservation</h1>
<p>Not reserved yet. Please double check the phrase<%-- and type in a password (optional), so reserved phrase will be unavailable to others. And to you, if you forgot the password.--%>
    and click Reserve button</p>
<form action="link.do" method="post">
    <label for="key"><strong>Key phrase:*</strong></label><br/>
    <input id="key" type="text" name="key" value="${param.originalKey}"/><br/>
    <%--<label for="pass">Password:</label><br/>--%>
    <%--<input id="pass" type="password" name="pass"/><br/>--%>
    <input type="submit" value="Reserve">
</form>
<hr/>
<%@ include file="WEB-INF/view/global/copyright.jsp" %>
</body>
</html>
