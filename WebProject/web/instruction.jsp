<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" href="/static/css/main.css"/>
    <title>${title}</title>
</head>
<body>
<a href="/" class="menu">Home</a><a href="addLink.jsp" class="menu">Add new link</a>
<h1>How it works</h1>
<h2>Step 1: Reserve a phrase</h2>
<p>Go to homepage, enter the phrase you'd like to use later for posting your link(s). Click 'Get Link' button to check
    if that phrase is not used yet.</p>
<p>If it isn't, the system suggests you to reserve the phrase automatically. Click 'Reserve', double check your phrase for
    errors, and click 'Reserve' again. The phrase is now exist in the system and waits for links to be attached.</p>
<h2>Step 2: Take a video and upload it to YouTube</h2>
<p>Please don't forget to set 'Unlisted' privacy mode (available only to those who have link), so video is not public.</p>
<h2>Step 3: Update your link</h2>
<p>Get your link from the homepage and edit it. Check your link is working by clicking on it yourself. Done.</p>
<hr/>
<%@ include file="WEB-INF/view/global/copyright.jsp" %>
</body>
</html>
