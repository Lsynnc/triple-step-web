package com.triplestep.config;

import com.triplestep.service.Service;

/**
 * @author Valentyn Markovych
 *         Created on 03.04.2016
 */
public interface ConfigService extends Service {

    // Application properties keys
    String DATA_DIR = "persistence.data.dir";

    String SHORTEST_KEY_LENGTH = "shortest.key.length";
    String BLOCK_SIZE = "block.size";
    String WRONG_SYMBOLS_PER_BLOCK = "wrong.symbols.per.block";


    void setObject(String key, Object value);


    Object getObject(String key);


    String getProperty(String key);


    String getProperty(String key, String defaultValue);
}
