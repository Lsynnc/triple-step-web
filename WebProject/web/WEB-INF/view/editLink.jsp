<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" href="/static/css/main.css"/>
    <title>${title}</title>
</head>
<body>
<a href="/" class="menu">Home</a>
<h1>${title}</h1>
<c:if test="${! empty errors}">
    <c:forEach var="error" varStatus="loop" items="${errors}">
        <div class="error">${error.message}</div>
        <br/>
    </c:forEach>
</c:if>
<c:if test="${empty errors}">
    <div>Key:</div>
    <div class="keyPhrase">${linkData.originalKey}</div>
    <form method="post">
        <input type="hidden" name="key" value="${linkData.ID}" disabled="true"/><br/>
        <c:forEach var="link" varStatus="loop" items="${linkData.links}">
            <label for="link${loop.index}">Link:</label><br/>
            <input id="link${loop.index}" type="text" name="link" value="${link}"/><br/>
        </c:forEach>
        <label for="linkN">Link:</label><br/>
        <input id="linkN" type="text" name="link" value="${link}"/><br/>
        <label for="linkM">Link:</label><br/>
        <input id="linkM" type="text" name="link" value="${link}"/><br/>
            <%-- TODO Add password protection --%>
            <%--<label for="pass">Password (lock link):</label><br/>--%>
            <%--<input id="pass" type="password" name="pass"/><br/>--%>
        <input type="submit" name="save" value="Save">
        <input type="submit" name="back" value="Back">
    </form>
</c:if>
<hr/>
<%@ include file="global/copyright.jsp" %>
</body>
</html>
