<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" href="/static/css/main.css"/>
    <title>${title}</title>
</head>
<body>
<a href="/" class="menu">Home</a>
<h1>${title}</h1>
<c:if test="${debugMode}">
    <c:forEach var="linkData" items="${linkDatas}">
        <div>${linkData}</div>
        <hr/>
    </c:forEach>
</c:if>
<hr/>
<%@ include file="global/copyright.jsp" %>
</body>
</html>
