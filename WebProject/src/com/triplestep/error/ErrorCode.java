package com.triplestep.error;

/**
 * @author Valentyn Markovych
 *         Created on 12.04.2016
 */
public enum ErrorCode {

    OK(0, "ok"),
    SERVER_ERROR(500, "Unknown server error");

    protected final int code;
    protected final String description;


    ErrorCode(int code, String description) {

        this.code = code;
        this.description = description;
    }


    public String getDescription() {

        return description;
    }


    public int getCode() {

        return code;
    }

    public static ErrorCode getByCode(int code) {

        for (ErrorCode errorCode : ErrorCode.values()) {
            if (code == errorCode.code) {
                return errorCode;
            }
        }
        return null;
    }
}
