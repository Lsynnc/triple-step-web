package com.triplestep.service.impl;

import com.triplestep.config.ConfigService;
import com.triplestep.data.LinkData;
import com.triplestep.service.PersistenceService;
import com.triplestep.service.SearchService;
import com.triplestep.service.ServiceManager;

import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;

/**
 * @author Valentyn Markovych
 *         Created on 01.04.2016
 */
public class LevenshteinSearchService implements SearchService {

    protected ServiceManager serviceManager;
    protected PersistenceService persistenceService;
    protected ConfigService configService;

    protected Collection<LinkData> linkDatas;

    protected int blockSize = 6;
    protected int wrongSymbolsPerBlock = 1;


    public LevenshteinSearchService() {

        serviceManager = ServiceManager.getInstance();
        configService = serviceManager.getService(ConfigService.class);
        loadSettings();
        persistenceService = serviceManager.getService(PersistenceService.class);
        linkDatas = persistenceService.loadAllLinks();
    }


    private void loadSettings() {

        blockSize = Integer.parseInt(configService.getProperty(ConfigService.BLOCK_SIZE,
                "" + blockSize));
        wrongSymbolsPerBlock = Integer.parseInt(configService.getProperty(ConfigService.WRONG_SYMBOLS_PER_BLOCK,
                "" + wrongSymbolsPerBlock));
    }


    @Override
    public void addLink(LinkData linkData) {

        linkDatas.add(linkData);
    }


    @Override
    public LinkData findSimilarLinks(String keyPhrase) {

        LinkData result = null;
        Iterator<LinkData> iterator = linkDatas.iterator();

        while (iterator.hasNext()) {
            final LinkData linkData = iterator.next();
            if (isSimilar(keyPhrase, linkData)) {
                result = linkData;
                break;
            }
        }

        return result;
    }


    @Override
    public Collection<LinkData> getAll() {

        return linkDatas;
    }


    @Override
    public LinkData getLink(String linkId) {

        UUID uuid = UUID.fromString(linkId);

        final Iterator<LinkData> linkDataIterator = linkDatas.iterator();
        while (linkDataIterator.hasNext()) {
            final LinkData nextLink = linkDataIterator.next();
            if (nextLink.getID().equals(uuid)) {
                return nextLink;
            }
        }

        return null;
    }


    private boolean isSimilar(String keyPhrase, LinkData linkData) {

        int keyLen = keyPhrase.length();

        if (keyLen <= 0) {
            return false;
        }

        String linkKey = linkData.getKey();
        int distance = levenshteinDistance(keyPhrase, linkKey);

        if (distance > ((keyLen / blockSize + 1) * wrongSymbolsPerBlock)) {
            return false;
        } else {
            return true;
        }
    }


    protected int levenshteinDistance(String lhs, String rhs) {

        int len0 = lhs.length() + 1;
        int len1 = rhs.length() + 1;

        // the array of distances
        int[] cost = new int[len0];
        int[] newcost = new int[len0];

        // initial cost of skipping prefix in String s0
        for (int i = 0; i < len0; i++) cost[i] = i;

        // dynamically computing the array of distances

        // transformation cost for each letter in s1
        for (int j = 1; j < len1; j++) {
            // initial cost of skipping prefix in String s1
            newcost[0] = j;

            // transformation cost for each letter in s0
            for (int i = 1; i < len0; i++) {
                // matching current letters in both strings
                int match = (lhs.charAt(i - 1) == rhs.charAt(j - 1)) ? 0 : 1;

                // computing cost for each transformation
                int cost_replace = cost[i - 1] + match;
                int cost_insert = cost[i] + 1;
                int cost_delete = newcost[i - 1] + 1;

                // keep minimum cost
                newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
            }

            // swap cost/newcost arrays
            int[] swap = cost;
            cost = newcost;
            newcost = swap;
        }

        // the distance is the cost for transforming all letters in both strings
        return cost[len0 - 1];
    }
}
