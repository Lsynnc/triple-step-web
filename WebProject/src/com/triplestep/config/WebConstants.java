package com.triplestep.config;

import java.util.Map;

/**
 * @author Valentyn Markovych
 *         Created on 01.04.2016
 */
public class WebConstants {

    private static final String PAGE_PREF = "/WEB-INF/view/";

    public static final String HOME_PAGE = "/";
    public static final String HOME_VIEW = PAGE_PREF + "home.jsp";

    public static final String LINK_PAGE = "/link.jsp";
    public static final String LINK_VIEW = PAGE_PREF + "link.jsp";

    public static final String LINKS_PAGE = "/links.jsp";
    public static final String LINKS_VIEW = PAGE_PREF + "links.jsp";

    public static final String EDIT_LINK_PAGE = "/editLink.jsp";
    public static final String EDIT_LINK_VIEW = PAGE_PREF + "editLink.jsp";


    public static final String PARAM_ID = "id"; // UID of a link
    public static final String PARAM_KEY = "key"; // Key phrase
    public static final String PARAM_LINK = "link"; // Data object itself
    public static final String PARAM_PASS = "pass"; // Password
    public static final String PARAM_ERROR = "err"; // Error code(s)
    public static final String PARAM_BACK = "back"; // For 'cancel' or 'back' button on forms
}
